//Bind data to the element. Alternative for ng-bind-html
app.directive("ngCustombind",function(){
	return {
		scope : {
			ngCustombind : "="
		},
		link : function(scope, elem, attr){			
			scope.$watch('ngCustombind', function(newValue, oldValue) {
				elem.html(scope.ngCustombind);
			});
		}
	}
});

app.directive("ngValidatedigit",[function(){
	return {
		scope : {
			ngValidatedigit : "="
		},
		link : function(scope, $elem, $attr, ctrl){
			console.log(scope);
			console.log($elem);

			$elem.on("blur",function(e){
				console.log(e);
				$elem[0].setCustomValidity("Not valid");
			});
		}
	}
}])
//DIRECTIVE FOR INPUT DATE FUNTIONALITIES
app.directive("ngInputDate",function($timeout){
	return {
		restrict : "A",
		scope : {
			ngInputDate : "=",
			ngModel : "="
		},
		link : function(scope, $elem, $attr){
			
			scope.ngInputDate = scope.ngInputDate || {};
			
			var id = scope.ngInputDate.id || "custid_"+Math.random(),
				dateElem;

			$elem.attr({"id":id});
				
			dateElem = new dhtmlXCalendarObject(id);
			dateElem.setDateFormat("%d-%M-%Y");
			dateElem.hideTime();

			if(scope.ngInputDate.value != undefined && scope.ngInputDate.value != "" && scope.ngInputDate.value != null){
				dateElem.setDate(new Date(scope.ngInputDate.value));
				$elem.val(dateElem.getFormatedDate("%d-%M-%Y", new Date(scope.ngInputDate.value)));
			}

			console.log(dateElem);
			//METHOD TO SHOW THE CALENDAR
			scope.ngInputDate.show = function(){
				
				//PUTTING TIMEOUT SO TO PREVENT IMMEDIATE HIDE BY EVENT BUBBLING
				setTimeout(function(){
					dateElem.show();
				},10);
				
			}
						
			//LISTEN TO USER SELECT DATE EVENT AND UPDATE MODEL ACCORDINGLY
			dateElem.attachEvent("onClick", function(date, state){
				scope.ngInputDate.value = dateElem.getDate();

				if(scope.ngModel)
					scope.ngModel = dateElem.getFormatedDate("%d-%M-%Y", new Date(scope.ngInputDate.value));

				scope.$apply();
			});
			
			//LISTENING TO OBJECT RESET REQUEST AND EMPTY THE INPUT FIELD
			scope.$watch('ngInputDate.value', function(newValue, oldValue) {
				if(newValue == "" || newValue == undefined){
					$elem.val("");
					
					if($elem.attr("required") != undefined)
						$elem[0].setCustomValidity("Please pick a date");
				} else{
					$elem[0].setCustomValidity("");
				}
			});
				
		}
	}
});
